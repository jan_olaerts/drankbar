package be.vdab.ExtraH5.oef1;

import java.util.Scanner;

public class DrankBar {

    public static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {

        while(true) {
            try {
                System.out.println("What is your first name: ");
                String firstName = scanner.next();

                System.out.println("What is your last name: ");
                String lastName = scanner.next();

                System.out.println("What is your age: ");
                int age = scanner.nextInt();

                printMenu(firstName);
                chooser(firstName, lastName, age);
                break;
            } catch (RuntimeException e) {
                break;
            } finally {
                scanner.nextLine();
            }
        }
    }

    public static void printMenu(String firstName) {

        System.out.println("Hello " + firstName + ", choose a drink: ");
        System.out.println("1. Water");
        System.out.println("2. Cola");
        System.out.println("3. Coffee");
        System.out.println("4. Beer");
        System.out.println("5. Nothing");
        System.out.println("6. Quit");
    }

    public static void chooser(String firstName, String lastName, int age) {

        int orderCount = 0;
        while(true) {
            System.out.println("Type in the number of your choice: ");
            int choice = scanner.nextInt();

            switch(choice) {
                case 1:
                    System.out.println("Here is your glass of water, " + firstName + " " + lastName);
                    orderCount++;
                    printMenu(firstName);
                    break;
                case 2:
                    System.out.println("Here is your Cola, " + firstName + " " + lastName);
                    orderCount++;
                    printMenu(firstName);
                    break;
                case 3:
                    System.out.println("Here is your coffee, " + firstName + " " + lastName);
                    orderCount++;
                    printMenu(firstName);
                    break;
                case 4:
                    if(age < 19) {
                        System.out.println("You are too young for a beer, " + firstName + " " + lastName);
                        printMenu(firstName);
                    }
                    else {
                        System.out.println("Here is your beer, " + firstName + " " + lastName);
                        orderCount++;
                        printMenu(firstName);
                    }
                    break;
                case 5:
                    System.out.println("You chose to drink nothing, " + firstName + " " + lastName);
                    printMenu(firstName);
                    break;
                case 6:
                    System.out.println("You ordered " + orderCount + " drinks");
                    return;
                default:
                    System.out.println("Please choose a valid option: ");
                    printMenu(firstName);
                    break;
            }
            System.out.println("You ordered " + orderCount + " drinks");
        }
    }
}